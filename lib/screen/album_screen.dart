import 'package:dotify_app/api/api.dart';
import 'package:dotify_app/model/model_dbase/OnlineAlbum.dart';
import 'package:dotify_app/pages/album_page.dart';
import 'package:flutter/material.dart';

class AlbumScreen extends StatelessWidget {
  final OnlineAlbum album;
  // final OnlineSong song;

  const AlbumScreen({Key key, @required this.album}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Container(
          child: ListTile(
            leading: Image.network(
              api +'viewFile/' +album.id.toString(),
              height: 50,
              width: 40,
            ),
            title: Text(album.name),
            subtitle: Text(album.userId.toString()),
            trailing: Padding(
              padding: const EdgeInsets.only(right: 10),
              child: InkWell(
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (BuildContext context) => AlbumPage(album: album,  )));
                  },
                  child: Image.asset('assets/icon/view_album_btn@2x.png')),
            ),
          ),
        ),
      ],
    );
  }
}
