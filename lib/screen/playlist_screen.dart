import 'package:dotify_app/model/playlist.dart';
import 'package:flutter/material.dart';

class PlaylistScreen extends StatelessWidget {
  final Playlist playlist;

  const PlaylistScreen({Key key, @required this.playlist}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Container(
          child: ListTile(
            title: Text(playlist.songname),
            subtitle: Text(playlist.artistname),
          ),
        ),
      ],
    );
  }
}
