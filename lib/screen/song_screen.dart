import 'package:dotify_app/data/scoped_modal/main.dart';
import 'package:dotify_app/model/music_model.dart';
import 'package:dotify_app/model/playlist.dart';
import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:share/share.dart';


class SongScreen extends StatefulWidget {
  final Music music;

  const SongScreen({Key key, @required this.music}) : super(key: key);

  @override
  _AlbumCardState createState() => _AlbumCardState();
}

class _AlbumCardState extends State<SongScreen> {
  @override
  Widget build(BuildContext context) {
    return ScopedModelDescendant(
      builder: (BuildContext context,Widget child,MainModel model){
                 return Column(
      children: <Widget>[
        Container(
          child: InkWell(onTap: (){},
                      child: ListTile(
              leading: Text(widget.music.numb.toString()),
              title: Text(widget.music.title),
              subtitle: Text(widget.music.subtitle),
              trailing: InkWell(
                  onTap: () {
                    _showMore();
                  },
                  child: Image.asset('assets/icon/more_info_btn@2x.png')),
            ),
          ),
        ),
      ],
    );
  
      },
    );}

  void _showMore() {
    showModalBottomSheet(
        builder: (BuildContext context) {
          return Container(
            height: 400,
            color: Colors.deepOrange,
            child: Padding(
              padding: const EdgeInsets.all(10.0),
              child: Column(
                children: <Widget>[
                  ListTile(
                  leading: CircleAvatar(
                    backgroundImage: AssetImage('assets/image/30.png'),
                  ),
                  title: Text('Fever'),
                  subtitle: Text('Wizkid'),
                ),
                SizedBox(height: 20,),
                  InkWell(
                    onTap: () {
                      Share.share('shared file');
                    },
                    child: Row(
                      children: <Widget>[
                        Icon(Icons.share),
                        SizedBox(
                          width: 8,
                        ),
                        Text('Share')
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Row(
                    children: <Widget>[
                      Icon(Icons.favorite),
                      SizedBox(
                        width: 8,
                      ),
                      Text('Favourites')
                    ],
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  InkWell(
                    onTap: () {
                      final Playlist playlist = new Playlist(
                          artistname: widget.music.title,
                          songname: widget.music.title);
                          playlists.add(playlist);
                    },
                    child: Row(
                      children: <Widget>[
                        Icon(Icons.playlist_add),
                        SizedBox(
                          width: 8,
                        ),
                        Text('Add to Playlist')
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Row(
                    children: <Widget>[
                      Icon(Icons.play_arrow),
                      SizedBox(
                        width: 8,
                      ),
                      Text('Play Next')
                    ],
                  )
                ],
              ),
            ),
          );
        },
        context: context);
  }
}
