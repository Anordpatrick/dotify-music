import 'package:dotify_app/model/music_model.dart';
import 'package:dotify_app/pages/artist_page.dart';
import 'package:flutter/material.dart';


class ArtistScreen extends StatelessWidget {
  final Music list;

  const ArtistScreen({Key key,@required this.list}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Container(
          child:     ListTile(
                      leading: Image.asset(
                        list.cover,
                        height: 50,
                        width: 40,
                      ),
                      title: Text(list.name),
                      // subtitle: Text(list.song),
                      trailing: Padding(
                        padding: const EdgeInsets.only(right: 10),
                        child: InkWell(
                          onTap: (){
                            Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) =>ArtistPage() ));
                          },
                          child: Image.asset('assets/icon/view_album_btn@2x.png')),
                      ),
                    ),
                    
        ),
      ],
    ); 
  }
}