import 'package:dotify_app/data/scoped_modal/main.dart';
import 'package:dotify_app/pages/home_page.dart';
import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  final MainModel _model = MainModel();

  @override
  
  Widget build(BuildContext context) {
    return ScopedModel<MainModel>(child:  MaterialApp(
     debugShowCheckedModeBanner: false,
      theme: ThemeData(
       
        primarySwatch: Colors.deepOrange
      ),
      home: HomePage(),
    )
  , model: _model,);
    }
}

