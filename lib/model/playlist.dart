import 'package:flutter/material.dart';

class Playlist {
  final String songname;
  final String artistname;

  Playlist({@required this.songname,@required this.artistname});
}


List<Playlist> playlists =<Playlist>[];