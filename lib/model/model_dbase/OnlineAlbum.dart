import 'package:flutter/material.dart';

class OnlineAlbum {
  final int id;
  final String name;
  final String cover;
  final String pathTofile;
  final int userId;
  final List<dynamic> songs;

  OnlineAlbum(
      {@required this.songs,
      @required this.userId,
      @required this.id,
      @required this.name,
      @required this.cover,
      @required this.pathTofile});

  OnlineAlbum.fromMap(Map<String, dynamic> map)
      : assert(map['user_id'] != null),
        assert(map['songs'] != null),
        assert(map['id'] != null),
        assert(map['name'] != null),
        assert(map['cover'] != null),
        assert(map['path_to_storage'] != null),
        id = map['id'],
        name = map['name'],
        cover = map['cover'],
        pathTofile = map['path_to_storage'],
        userId = map['user_id'],
        songs = map['songs'];
}
