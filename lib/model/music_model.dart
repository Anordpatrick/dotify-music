import 'package:flutter/foundation.dart';

class Music {
  final String cover;
  final String title;
  final String subtitle;
  final String icon;
  final String text;
  final String album;
  final String song;
  final String no;
  final String numb;
  final String name;

  Music(
      {@required this.name,
        @required this.song,
      @required this.album,
      @required this.text,
      @required this.icon,
      @required this.cover,
      @required this.title,
      @required this.subtitle,
      @required this.numb,
      @required this.no});
}

List<Music> musics = <Music>[
  Music(
      cover: 'assets/image/20.png',
      subtitle: 'Today top song',
      title: 'Top',
      icon: 'assets/icon/pop_mic_icon.png',
      text: 'Pop',
      album: 'A boy From Tandale',
      song: 'Love You Die',
      no: '6473689',
       numb: '2', name: 'Diamond Platnumz'),
  Music(
      cover: 'assets/image/19.png',
      subtitle: 'Viral Hits',
      title: 'Viral',
      icon: 'assets/icon/rock_fire_icon.png',
      text: 'Rock',
      album: 'The Queen is Dead',
      song: 'Leave Me Alone', no: '2866938', numb: '3', name: 'Khaligraphic Jones'),
  Music(
      cover: 'assets/image/14.png',
      subtitle: 'New Hiphop',
      title: 'Hip hop',
      icon: 'assets/icon/radio_icon.png',
      text: 'Egypt',
      album: 'The Carters',
      song: 'Pump in up', no: '208937553', numb: '4', name: 'Jay Z'),
  Music(
      cover: 'assets/image/23.png',
      subtitle: 'Old school',
      title: 'Old',
      icon: 'assets/icon/playlists_btn@2x.png',
      text: 'Bomba',
      album: 'Invasion of Privacy',
      song: 'Bordak Yellow', no: '736355', numb: '5', name: 'Card B'),
  Music(
      cover: 'assets/image/33.png',
      subtitle: 'Old school',
      title: 'Old',
      icon: 'assets/icon/playlists_btn@2x.png',
      text: 'Bomba',
      album: 'Father of Four',
      song: 'Bad And Boujoe', no: '5345273', numb: '6', name: 'Offset'),
  Music(
      cover: 'assets/image/23.png',
      subtitle: 'Old school',
      title: 'Old',
      icon: 'assets/icon/playlists_btn@2x.png',
      text: 'Bomba',
      album: 'Star Boy',
      song: 'Fever', no: '25374683', numb: '7', name: 'Wizkid'),
  Music(
      cover: 'assets/image/18.png',
      subtitle: 'Old school',
      title: 'Old',
      icon: 'assets/icon/playlists_btn@2x.png',
      text: 'Bomba',
      album: 'Cash Madam',
      song: 'In Case You Dont Know', no: '89387635', numb: '8', name: 'Jux'),
  Music(
      cover: 'assets/image/21.png',
      subtitle: 'Old school',
      title: 'Old',
      icon: 'assets/icon/playlists_btn@2x.png',
      text: 'Bomba',
      album: 'Father Of Asaad',
      song: 'Higher', no: '2453768', numb: '9', name: 'Dj Khaleed'),
];
