import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:dotify_app/api/api.dart';
import 'package:dotify_app/model/model_dbase/OnlineAlbum.dart';
import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:audioplayers/audioplayers.dart';

mixin DotifyConnectedModel on Model {
  List<OnlineAlbum> _availableAlbums;
  // List<OnlineSong> _availableSongs;
  Map<String, dynamic> songs;
  bool _isplayed = false;
 
}
mixin MusicScreenModel on DotifyConnectedModel {}


mixin SongModel on DotifyConnectedModel {
  AudioPlayer audioPlayer = AudioPlayer();

  play() async {
    int result = await audioPlayer.play(api + 'viewSongFile/1');
    if (result == 1) {
      // success
    }
  }

  pause() async {
    int result = await audioPlayer.pause();
    if (result == 1) {
      // success

    }
  }

// set music to play
  void setisPlayed(bool play) {
    _isplayed = play;
    notifyListeners();
  }

  // method to get

  bool get isPlayed => _isplayed;

  // method to pull songs from databse
  // Future<void> fetchedSongs() async {
  //      final List<OnlineSong> _fetchedSongs =[];
  //      try{
  //           final http.Response response =await http.get(api +'songs');
  //           Map<String,dynamic> data= json.decode(response.body);

  //           if(response.statusCode==200){
  //             data['songs'].forEach((songs){
  //               final song =OnlineSong.fromMap(songs);
  //               _fetchedSongs.add(song);
  //             });
  //           }else{
  //             print(response.statusCode);
  //           }

  //      }catch(e){
  //        print(e);
  //      }

  //     _availableSongs = _fetchedSongs;
  //     print(_availableSongs);
  //     notifyListeners();
  // }

  // List<OnlineSong> getAvailableSongs() {
  //     if(_availableSongs==null){
  //       return <OnlineSong>[];
  //     }
  //       return _availableSongs;
      
  // }

  AudioPlayer get audioPlayerInstance => audioPlayer;

}

mixin AlbumModel on DotifyConnectedModel {
  // method to pull album from the network
  Future<void> fetchedAlbums() async {
    final List<OnlineAlbum> _fetchedAlbums = [];
    try {
      final http.Response response = await http.get(api + 'albums');
      Map<String, dynamic> data = json.decode(response.body);
      if (response.statusCode == 200) {
        data['albums'].forEach((albums) {
          final album = OnlineAlbum.fromMap(albums);
          _fetchedAlbums.add(album);
        });
      } else {
        print(response.statusCode);
      }
    } catch (e) {
      print(e);
    }
    _availableAlbums = _fetchedAlbums;
    print(_availableAlbums);
    notifyListeners();
  }

  // getter of album
  List<OnlineAlbum> getAvailableAlbums() {
    if (_availableAlbums == null) {
      return <OnlineAlbum>[];
    }
    return _availableAlbums;
  }

  // method to get album cover
  String getalbumcover({@required int albumId}) {
    return api + 'viewFile/' + albumId.toString();
  }

  // method to get song cover
  String getSongcover({@required int songId}) {
    return api + 'viewCoverFile/' + songId.toString();
  }
}
