import 'package:dotify_app/data/scoped_modal/main.dart';
import 'package:dotify_app/model/model_dbase/OnlineAlbum.dart';
import 'package:dotify_app/model/music_model.dart';
import 'package:dotify_app/model/playlist.dart';
import 'package:dotify_app/pages/now_playing_page.dart';
import 'package:flutter/material.dart';
import 'package:share/share.dart';

class SongsCard extends StatefulWidget {
  final OnlineAlbum album;
  final int index;
  final MainModel model;
  // final OnlineSong song;

  const SongsCard({Key key, @required this.album,@required this.index, this.model}) : super(key: key);

  @override
  _AlbumCardState createState() => _AlbumCardState();
}

class _AlbumCardState extends State<SongsCard> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Container(
          child: InkWell(onTap: (){
                  Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) =>NowPlaying(playing: musics.removeLast(), model: widget.model,)));
                },
                      child: ListTile(
              leading: Text(widget.index.toString()),
              title: Text(widget.album.name),
              subtitle: Text(widget.index.toString()),
              trailing: InkWell(
                  onTap: () {
                    _showMore();
                  },
                  child: Image.asset('assets/icon/more_info_btn@2x.png')),
            ),
          ),
        ),
      ],
    );
  }

  void _showMore() {
    showModalBottomSheet(
        builder: (BuildContext context) {
          return Container(
            height: 400,
            color: Colors.deepOrange,
            child: Padding(
              padding: const EdgeInsets.all(10.0),
              child: Column(
                children: <Widget>[
                  ListTile(
                  leading: CircleAvatar(
                    backgroundImage: AssetImage('assets/image/30.png'),
                  ),
                  title: Text('Fever'),
                  subtitle: Text('Wizkid'),
                ),
                SizedBox(height: 20,),
                  InkWell(
                    onTap: () {
                      Share.share('Drey');
                    },
                    child: Row(
                      children: <Widget>[
                        Icon(Icons.share),
                        SizedBox(
                          width: 8,
                        ),
                        Text('Share')
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Row(
                    children: <Widget>[
                      Icon(Icons.favorite),
                      SizedBox(
                        width: 8,
                      ),
                      Text('Favourites')
                    ],
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  InkWell(
                    onTap: () {
                      final Playlist playlist = new Playlist(
                          artistname: widget.album.name,
                          songname: widget.album.songs[widget.index]['title']);
                          playlists.add(playlist);
                    },
                    child: Row(
                      children: <Widget>[
                        Icon(Icons.playlist_add),
                        SizedBox(
                          width: 8,
                        ),
                        Text('Add to Playlist')
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Row(
                    children: <Widget>[
                      Icon(Icons.play_arrow),
                      SizedBox(
                        width: 8,
                      ),
                      Text('Play Next')
                    ],
                  )
                ],
              ),
            ),
          );
        },
        context: context);
  }
}
