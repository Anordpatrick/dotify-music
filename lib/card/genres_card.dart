import 'package:dotify_app/model/music_model.dart';
import 'package:flutter/material.dart';

class GenresCard extends StatelessWidget {
  final Music musics;

  const GenresCard({Key key, @required this.musics}) : super(key: key);
  @override
  Widget build(BuildContext context) {
     return Container(
      child: Padding(
        padding: const EdgeInsets.only(left: 10, ),
        child: Container(
          color: Colors.white,
          height: 150,
          width: 150,
          child: Column(
            children: <Widget>[
              Stack(
                children: <Widget>[
                  Container(decoration: 
                  BoxDecoration(
                     color: Colors.deepOrange.withOpacity(0.8)
                     
                  ),
                   
                    height: 140,
                   
                  ),
                  Padding(
                    padding: const EdgeInsets.all(40.0),
                    child: Center(child: Image.asset(musics.icon)),
                  )
                 
                ],
              ),
              SizedBox(
                height: 18,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 2),
                child: Text(
                  musics.text,
                  style: TextStyle(color: Colors.black),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
