import 'package:dotify_app/model/background.dart';
import 'package:dotify_app/pages/browse_page.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class SignupPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Background(
      scaffold: Scaffold(
        appBar: AppBar(
          elevation: 0,
          backgroundColor: Colors.transparent,
          leading: IconButton(
            icon: Icon(
              Icons.arrow_back_ios,
              color: Colors.grey[500],
            ),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
          title: Padding(
            padding: const EdgeInsets.only(left: 30,top: 10,bottom: 8,right:25 
            ),
            child: Container(child: Image.asset('assets/icon/dotify_icon.png',)),
          ),
        ),
        body: SingleChildScrollView(
          child: Container(
            child: Column(
              children: <Widget>[
                Container(
                  height: 60,
                  color: Colors.blue,
                  child: Row(
                    children: <Widget>[
                      FlatButton.icon(
                        icon: Icon(
                          FontAwesomeIcons.facebookF,
                          color: Colors.white,
                        ),
                        label: Padding(
                          padding: const EdgeInsets.only(left: 40),
                          child: Text(
                            'Sign up with Facebook',
                            style: TextStyle(color: Colors.white, fontSize: 18),
                          ),
                        ),
                        onPressed: () {},
                      )
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(
                    top: 20,
                  ),
                  child: Text(
                    'or with Email',
                    style: TextStyle(color: Colors.grey),
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                Form(
                  child: Column(
                    children: <Widget>[
                      TextFormField(
                        keyboardType: TextInputType.text,
                        decoration: InputDecoration(
                          fillColor: Colors.white,
                          filled: true,
                          prefixIcon: Icon(Icons.mail),
                          labelText: "Email",
                        ),
                      ),
                      SizedBox(
                        height: 3,
                      ),
                      TextFormField(
                        keyboardType: TextInputType.text,
                        decoration: InputDecoration(
                          fillColor: Colors.white,
                          filled: true,
                          prefixIcon: Icon(Icons.person),
                          labelText: "Choose username",
                        ),
                      ),
                      SizedBox(
                        height: 3,
                      ),
                      TextFormField(
                        keyboardType: TextInputType.text,
                        decoration: InputDecoration(
                          fillColor: Colors.white,
                          filled: true,
                          prefixIcon: Icon(Icons.lock),
                          labelText: "Choose password",
                        ),
                      ),
                      SizedBox(
                        height: 3,
                      ),
                      TextFormField(
                        keyboardType: TextInputType.text,
                        decoration: InputDecoration(
                          fillColor: Colors.white,
                          filled: true,
                          prefixIcon: Icon(Icons.calendar_today),
                          labelText: "Data of birth",
                        ),
                      ),
                      SizedBox(
                        height: 3,
                      ),
                      TextFormField(
                        keyboardType: TextInputType.text,
                        decoration: InputDecoration(
                          fillColor: Colors.white,
                          filled: true,
                          prefixIcon: Icon(Icons.person),
                          labelText: "Gender",
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
        backgroundColor: Colors.transparent,
        bottomNavigationBar: BottomAppBar(
          color: Colors.deepOrange,
          child: FlatButton(
              color: Colors.deepOrange,
              child: Text(
                'SIGN UP',
                style: TextStyle(fontSize: 20, color: Colors.white),
              ),
              onPressed: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (BuildContext context) => BrowsePage()));
              }),
        ),
      ),
      sigmaX: 10,
      sigmaY: 10,
    );
  }
}
