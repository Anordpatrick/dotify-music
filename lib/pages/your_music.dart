import 'package:dotify_app/data/scoped_modal/main.dart';
import 'package:dotify_app/model/background.dart';
import 'package:dotify_app/model/music_model.dart';
import 'package:dotify_app/model/playlist.dart';
import 'package:dotify_app/pages/now_playing_page.dart';
import 'package:dotify_app/screen/album_screen.dart';
import 'package:dotify_app/screen/artist_screen.dart';
import 'package:dotify_app/screen/playlist_screen.dart';
import 'package:dotify_app/screen/song_screen.dart';
import 'package:flutter/material.dart';


class MusicPage extends StatefulWidget {
  final MainModel model;
   

  const MusicPage({Key key, this.model}) : super(key: key);
  @override
  _MusicPageState createState() => _MusicPageState();
}

class _MusicPageState extends State<MusicPage> {
  int index;
  @override
  initState(){
    super.initState();
    widget.model.fetchedAlbums();
  }
  int _index = 0;

  void _checkedindex(index) {
    setState(() {
      _index = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Background(
      scaffold: Scaffold(
        backgroundColor: Colors.white54,
        appBar: AppBar(
          elevation: 0,
          backgroundColor: Colors.transparent,
          leading: Image.asset('assets/icon/menu_btn.png'),
          title: Text(
            'YOUR MUSIC',
            style: TextStyle(color: Colors.black),
          ),
          actions: <Widget>[Image.asset('assets/icon/search_btn.png')],
        ),
        body: CustomScrollView(
          slivers: <Widget>[
            SliverList(
              delegate: SliverChildListDelegate([
                Row(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.all(5.0),
                      child: Container(
                        height: 75,
                        width: 75,
                        child: InkWell(
                          onTap: () {
                            _checkedindex(0);
                          },
                          child: Image.asset(
                            _index == 0
                                ? 'assets/icon/artists_btn_1@3x.png'
                                : 'assets/icon/artists_btn.png',
                            fit: BoxFit.cover,
                          ),
                        ),
                        color: Colors.white,
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(5.0),
                      child: Container(
                        height: 75,
                        width: 75,
                        child: InkWell(
                          onTap: () {
                            _checkedindex(1);
                          },
                          child: Image.asset(
                            _index == 1
                                ? 'assets/icon/album_btn.png'
                                : 'assets/icon/album_btn_1@3x.png',
                            fit: BoxFit.cover,
                          ),
                        ),
                        color: Colors.white,
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(5.0),
                      child: Container(
                        height: 75,
                        width: 75,
                        child: InkWell(
                          onTap: () {
                            _checkedindex(2);
                          },
                          child: Image.asset(
                            _index == 2
                                ? 'assets/icon/songs_btn@3x_1.png'
                                : 'assets/icon/songs_btn@3x.png',
                            fit: BoxFit.cover,
                          ),
                        ),
                        color: Colors.white,
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(5.0),
                      child: Container(
                        height: 75,
                        width: 75,
                        child: InkWell(
                            onTap: () {
                              _checkedindex(3);
                            },
                            child: Image.asset(
                              _index == 3
                                  ? 'assets/icon/playlists_btn@3x_1.png'
                                  : 'assets/icon/playlists_btn@2x.png',
                              fit: BoxFit.cover,
                            )),
                        color: Colors.white,
                      ),
                    ),
                    Divider(
                      indent: 20,
                    )
                  ],
                )
              ]),
            ),
            _index == 0
                ? SliverList(
                    delegate: SliverChildBuilderDelegate(
                        (BuildContext context, int index) {
                      return ArtistScreen(
                        list: musics[index],
                      );
                    }, childCount: musics.length),
                  )
                : _index == 1
                    ? SliverList(
                        delegate: SliverChildBuilderDelegate(
                            (BuildContext context, int index) {
                          return AlbumScreen(
                             album: widget.model.getAvailableAlbums()[index],
                          );
                        }, childCount: widget.model.getAvailableAlbums().length),
                      )
                    : _index == 2 ?
                    SliverList(
                        delegate: SliverChildBuilderDelegate(
                            (BuildContext context, int index) {
                          return 
                          SongScreen(music: musics[index],
                          );
                        }, 
                        childCount: musics.length
                        ),
                      )
                      
                        : SliverList(
                            delegate: SliverChildBuilderDelegate(
                                (BuildContext context, int index) {
                              return PlaylistScreen(
                                playlist: playlists[index],
                              );
                            }, childCount: playlists.length),
                          )
          ],
        ),
        bottomNavigationBar: BottomAppBar(
          child: InkWell(
            onTap: () {
              
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (BuildContext context) => NowPlaying(playing: musics.first, model: widget.model,  )));
            },
            child: ListTile(
              leading: CircleAvatar(
                backgroundImage: AssetImage('assets/image/26.png'),
              ),
              title: Text('Leave Me Alone'),
              subtitle: Text('Khaligraphic Jones'),
              trailing: Image.asset('assets/icon/pause_btn.png'),
            ),
          ),
        ),
      ),
      sigmaX: 55,
      sigmaY: 55,
    );
  }
}
