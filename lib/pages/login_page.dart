import 'package:dotify_app/model/background.dart';
import 'package:dotify_app/pages/browse_page.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  FocusNode _usernamefocusNode = FocusNode();
  FocusNode _passwordfocusNode = FocusNode();
  TextEditingController _usernameTexteditingControler = TextEditingController();
  TextEditingController _passwordTexreditingController =
      TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Background(
      scaffold: Scaffold(
        appBar: AppBar(
          elevation: 0,
          backgroundColor: Colors.white,
          leading: IconButton(
            icon: Icon(
              Icons.arrow_back_ios,
              color: Colors.black,
            ),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
          title: Padding(
            padding:
                const EdgeInsets.only(left: 30, top: 10, bottom: 8, right: 25),
            child: Container(child: Image.asset('assets/icon/dotify_icon.png')),
          ),
        ),
        body: SingleChildScrollView(
          child: Container(
            child: Column(
              children: <Widget>[
                Container(
                  height: 60,
                  color: Colors.blue,
                  child: Row(
                    children: <Widget>[
                      FlatButton.icon(
                        icon: Icon(
                          FontAwesomeIcons.facebookF,
                          color: Colors.white,
                        ),
                        label: Padding(
                          padding: const EdgeInsets.only(left: 40),
                          child: Text(
                            'Log in with facebook ',
                            style: TextStyle(color: Colors.white, fontSize: 20),
                          ),
                        ),
                        onPressed: () {},
                      )
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(
                    top: 20,
                  ),
                  child: Text(
                    'or',
                    style: TextStyle(color: Colors.grey),
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                Form(
                  child: Column(
                    children: <Widget>[
                      TextFormField(
                        controller: _usernameTexteditingControler,
                        focusNode: _usernamefocusNode,
                        validator: (value) {
                          if (value.isEmpty)
                            return "Username is required";
                          else
                            return null;
                        },
                        keyboardType: TextInputType.text,
                        decoration: InputDecoration(
                          fillColor: Colors.white,
                          filled: true,
                          prefixIcon: Icon(Icons.person),
                          labelText: "Username",
                        ),
                      ),
                      SizedBox(
                        height: 3,
                      ),
                      TextFormField(
                        controller: _passwordTexreditingController,
                        focusNode: _passwordfocusNode,
                        keyboardType: TextInputType.text,
                        decoration: InputDecoration(
                          fillColor: Colors.white,
                          filled: true,
                          prefixIcon: Icon(Icons.lock),
                          labelText: "Password",
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 5,
                ),
                Text(
                  'Forgot your password?',
                  style: TextStyle(color: Colors.black87),
                )
              ],
            ),
          ),
        ),
        backgroundColor: Colors.transparent,
        bottomNavigationBar: BottomAppBar(
          color: Colors.deepOrange,
          child: FlatButton(
              color: Colors.deepOrange,
              child: Text(
                'LOG IN',
                style: TextStyle(fontSize: 20, color: Colors.white),
              ),
              onPressed: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (BuildContext context) => BrowsePage()));
              }),
        ),
      ),
      sigmaX: 10,
      sigmaY: 10,
    );
  }
}
