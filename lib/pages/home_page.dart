import 'package:dotify_app/model/background.dart';
import 'package:dotify_app/pages/login_page.dart';
import 'package:dotify_app/pages/signup_page.dart';
import 'package:flutter/material.dart';


class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Background(scaffold: Scaffold(
        backgroundColor: Colors.transparent,
        body: Container(
          child: Column(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(35.0),
                child: Container(
                  child: Image.asset('assets/icon/dotify_icon.png'),
                ),
              ),
              SizedBox(
                height: 200,
              ),
              Container(
                height: 100,
                width: MediaQuery.of(context).size.width,
                color: Colors.deepOrange[400],
                child: Align(
                  alignment: Alignment.bottomCenter,
                  child: Text(
                    'Your Music',
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 45,
                        fontWeight: FontWeight.bold),
                  ),
                ),
              ),
              Container(
                height: 50,
                width: MediaQuery.of(context).size.width,
                color: Colors.deepOrange.withOpacity(0.4),
                child: Align(
                  alignment: Alignment.bottomCenter,
                  child: Text(
                    'Turned to you. ',
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 33,
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
        bottomNavigationBar: BottomAppBar(
          child: Row(
            children: <Widget>[
              Expanded(
                child: FlatButton(
                  child: Text(
                    'LOG IN',
                    style: TextStyle(fontSize: 20),
                  ),
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => LoginPage()),
                    );
                  },
                ),
              ),
              Expanded(
                child: Container(
                  color: Colors.deepOrange,
                  child: FlatButton(
                    child: Text(
                      'SIGN UP',
                      style: TextStyle(fontSize: 20, color: Colors.white),
                    ),
                    onPressed: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (BuildContext context) => SignupPage()));
                                                  },
                                                ),
                                              ),
                                            )
                                          ],
                                        ),
                                      ),
                                    ),
                                   sigmaX: 10, sigmaY: 10,
                                    
                                  );
                                }
                              
}
