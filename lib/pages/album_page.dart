import 'package:dotify_app/card/songs_card.dart';
import 'package:dotify_app/data/scoped_modal/main.dart';
import 'package:dotify_app/model/background.dart';
import 'package:dotify_app/model/model_dbase/OnlineAlbum.dart';
import 'package:dotify_app/model/music_model.dart';
import 'package:dotify_app/pages/now_playing_page.dart';
import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';

class AlbumPage extends StatefulWidget {
  final OnlineAlbum album;
  
  // final OnlineSong song;

  const AlbumPage({Key key, @required this.album}) : super(key: key);

  @override
  _AlbumPageState createState() => _AlbumPageState();
}

class _AlbumPageState extends State<AlbumPage> {
  @override
  Widget build(BuildContext context) {
    return ScopedModelDescendant(
      builder: (BuildContext context, Widget child, MainModel model) {
        return Background(
          scaffold: Scaffold(
            backgroundColor: Colors.white70,
            appBar: PreferredSize(
              child: Padding(
                padding: const EdgeInsets.all(10.0),
                child: Stack(
                  children: <Widget>[
                    Container(
                      decoration: BoxDecoration(
                        image: DecorationImage(
                            image: NetworkImage(
                                model.getalbumcover(albumId: widget.album.id)),fit: BoxFit.cover
                                ),
                      ),
                      height: 260,
                      width: MediaQuery.of(context).size.width,
                      child: Column(
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.only(
                              left: 10,
                              top: 25,
                            ),
                            child: Row(
                              children: <Widget>[
                                IconButton(
                                  icon: Image.asset(
                                      'assets/icon/back_btn_light@2x.png'),
                                  onPressed: () {
                                    Navigator.pop(context);
                                  },
                                ),
                                SizedBox(
                                  width: 48,
                                ),
                                Column(
                                  children: <Widget>[
                                    Text(
                                      'Album',
                                      style: TextStyle(color: Colors.white),
                                    ),
                                    Text(
                                      'Sawkers In Sawkers',
                                      style: TextStyle(color: Colors.white),
                                    )
                                  ],
                                )
                              ],
                            ),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Stack(
                            children: <Widget>[
                              CircleAvatar(
                                radius: 40,
                                backgroundImage: NetworkImage(model
                                    .getalbumcover(albumId: widget.album.id)),
                              ),
                             
                            ],
                          ),
                          SizedBox(
                            height: 30,
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 110),
                            child: Row(
                              children: <Widget>[
                                Image.asset('assets/icon/heart_song_btn.png'),
                                InkWell(
                                    onTap: () {
                                      model.play();
                                    },
                                    child: Image.asset(
                                        'assets/icon/play_artist_btn.png')),
                                InkWell(
                                    onTap: () {
                                    },
                                    child: Image.asset(
                                        'assets/icon/share_music_btn.png'))
                              ],
                            ),
                          )
                        ],
                      ),
                    )
                  ],
                ),
              ),
              preferredSize: Size(200, 260),
            ),
            body: CustomScrollView(
              slivers: <Widget>[
                SliverList(
                  delegate: SliverChildListDelegate([
                    Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: Text('TRACKLIST'),
                    ),
                    Divider()
                  ]),
                ),
                SliverList(
                  delegate: SliverChildBuilderDelegate(
                      (BuildContext context, int index) {
                    return SongsCard(
                      album: widget.album,
                      index: index, 
                    );
                  }, childCount: widget.album.songs.length),
                )
              ],
            ),
            bottomNavigationBar: BottomAppBar(
                child: FlatButton(
              child: ListTile(
                leading: CircleAvatar(
                  backgroundImage: AssetImage('assets/image/30.png'),
                ),
                title: Text(" I Won't Let You Down"),
                subtitle: Text('Ok Go'),
                trailing: Image.asset('assets/icon/pause_btn.png'),
              ),
              onPressed: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (BuildContext context) => NowPlaying(
                              playing: musics.removeLast(), model: model, 
                            )));
              },
            )),
          ),
          sigmaX: 60,
          sigmaY: 60,
        );
      },
    );
  }
}
