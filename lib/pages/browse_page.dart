import 'dart:ui';
import 'package:dotify_app/card/genres_card.dart';
import 'package:dotify_app/card/music_types_card.dart';
import 'package:dotify_app/data/scoped_modal/main.dart';
import 'package:dotify_app/model/background.dart';
import 'package:dotify_app/model/model_dbase/OnlineAlbum.dart';
import 'package:dotify_app/model/music_model.dart';
import 'package:dotify_app/pages/now_playing_page.dart';
import 'package:dotify_app/pages/your_music.dart';
import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';


class BrowsePage extends StatelessWidget {
  final OnlineAlbum album;
  final MainModel model;


  const BrowsePage({Key key, this.album, this.model}) : super(key: key);
  @override
  Widget build(BuildContext context) {
     return ScopedModelDescendant(builder: (BuildContext context, Widget child, MainModel model) {
       return Background(
          scaffold: Scaffold(
            backgroundColor: Colors.white24,
            appBar: AppBar(
              elevation: 0.3,
              backgroundColor: Colors.transparent,
              title: Padding(
                padding:
                    const EdgeInsets.only(left: 30, top: 10, bottom: 8, right: 25),
                child:
                    Container(child: Image.asset('assets/icon/dotify_icon@3x.png')),
              ),
              actions: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Icon(
                    Icons.search,
                    size: 40,
                  ),
                )
              ],
            ),
            drawer: Drawer(
              child: Scaffold(
                body: Container(
                  decoration: BoxDecoration(
                      image: DecorationImage(
                          image: AssetImage('assets/image/40.png'),
                          fit: BoxFit.cover,
                          // colorFilter: ColorFilter.mode(
                          //     Colors.deepOrange,
                          //     BlendMode.overlay
                          //     )
                              )
                              ),
                  child: Column(
                    children: <Widget>[
                      Padding(
                        padding:
                            const EdgeInsets.only(left: 40, top: 40, right: 15),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Image.asset('assets/icon/dotify_logo_light@2x.png'),
                            Image.asset('assets/icon/Settings_btn.png')
                          ],
                        ),
                      ),
                      CircleAvatar(
                        radius: 42,
                        backgroundColor: Colors.white,
                        child: CircleAvatar(
                          radius: 40,
                          backgroundImage: AssetImage('assets/image/10.png'),
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Text(
                        'Patrick Anordy',
                        style: TextStyle(color: Colors.white, fontSize: 20),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Row(
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.only(left: 10),
                            child: FlatButton.icon(
                              icon: Image.asset('assets/icon/Browse_icon.png'),
                              label: Container(
                                  margin: EdgeInsets.only(left: 15),
                                  child: Text(
                                    'BROWSE',
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontWeight: FontWeight.bold,
                                        fontSize: 25),
                                  )),
                              onPressed: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (BuildContext context) =>
                                            BrowsePage()));
                              },
                            ),
                          )
                        ],
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      Row(
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.only(left: 10),
                            child: FlatButton.icon(
                              icon: Image.asset('assets/icon/activity_icon.png'),
                              label: Container(
                                  margin: EdgeInsets.only(left: 15),
                                  child: Text(
                                    'ACTIVITY',
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontWeight: FontWeight.bold,
                                        fontSize: 25),
                                  )),
                              onPressed: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (BuildContext context) =>
                                            BrowsePage()));
                              },
                            ),
                          )
                        ],
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      Row(
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.only(left: 10),
                            child: FlatButton.icon(
                              icon: Image.asset('assets/icon/radio_icon.png'),
                              label: Container(
                                  margin: EdgeInsets.only(left: 15),
                                  child: Text(
                                    'RADIO',
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontWeight: FontWeight.bold,
                                        fontSize: 25),
                                  )),
                              onPressed: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (BuildContext context) =>
                                            BrowsePage()));
                              },
                            ),
                          )
                        ],
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      Row(
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.only(left: 10),
                            child: FlatButton.icon(
                              icon:
                                  Image.asset('assets/icon/music_library_icon.png'),
                              label: Container(
                                  margin: EdgeInsets.only(left: 15),
                                  child: Text(
                                    'YOUR MUSIC',
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontWeight: FontWeight.bold,
                                        fontSize: 25),
                                  )),
                              onPressed: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (BuildContext context) =>
                                            MusicPage(model: model,)));
                              },
                            ),
                          )
                        ],
                      )
                    ],
                  ),
                ),
              ),
            ),
            body: CustomScrollView(
              slivers: <Widget>[
                SliverList(
                  delegate: SliverChildListDelegate([
                    Align(
                      alignment: Alignment.bottomCenter,
                      child: Text(
                        'Browse',
                        style: TextStyle(fontSize: 30),
                      ),
                    )
                  ]),
                ),
                SliverToBoxAdapter(
                  child: Container(
                    height: 180,
                    child: ListView.builder(
                      scrollDirection: Axis.horizontal,
                      itemBuilder: (BuildContext context, int index) {
                        return MusicType(
                          musics: musics[index],
                        );
                      },
                      itemCount: musics.length,
                    ),
                  ),
                ),
                SliverList(
                  delegate: SliverChildListDelegate([
                    Padding(
                      padding: const EdgeInsets.only(left: 10, top: 2),
                      child: Text('FEATURED ALBUM'),
                    ),
                    Divider()
                  ]),
                ),
                SliverList(
                  delegate: SliverChildListDelegate([
                    ListTile(
                      leading: Image.asset(
                        'assets/image/33.png',
                        height: 50,
                        width: 40,
                      ),
                      title: Text('Variable'),
                      subtitle: Text('Swifthead'),
                      trailing: Padding(
                        padding: const EdgeInsets.only(right: 10),
                        child: Image.asset('assets/icon/view_album_btn@2x.png'),
                      ),
                    ),
                    Divider()
                  ]),
                ),
                SliverList(
                  delegate: SliverChildListDelegate([
                    Column(
                      children: <Widget>[
                        Align(
                            alignment: Alignment.topLeft,
                            child: Padding(
                              padding: const EdgeInsets.only(left: 10),
                              child: Text('GENRES MOODS'),
                            )),
                        Slider(
                          onChanged: (double value) {},
                          value: 0.2,
                          inactiveColor: Colors.white,
                          activeColor: Colors.deepOrange,
                        )
                      ],
                    )
                  ]),
                ),
                SliverToBoxAdapter(
                  child: Container(
                    height: 180,
                    child: ListView.builder(
                      scrollDirection: Axis.horizontal,
                      itemBuilder: (BuildContext context, int index) {
                        return GenresCard(
                          musics: musics[index],
                        );
                      },
                      itemCount: musics.length,
                    ),
                  ),
                ),
              ],
            ),
            bottomNavigationBar: BottomAppBar(
              child: InkWell(
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (BuildContext context) => NowPlaying( model: model, playing: musics.first, )));
                },
                child: ListTile(
                  leading: CircleAvatar(
                    backgroundImage: AssetImage('assets/image/32.png'),
                  ),
                  title: Text("In Case You Don't Know"),
                  subtitle: Text('Jux'),
                  trailing: Image.asset('assets/icon/pause_btn.png'),
                ),
              ),
            ),
          ),
          sigmaX: 18,
          sigmaY: 18,
        );
      
       
     },);}
    }
    
 