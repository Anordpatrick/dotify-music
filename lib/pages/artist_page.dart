import 'dart:ui';

import 'package:dotify_app/data/scoped_modal/main.dart';
import 'package:dotify_app/model/background.dart';
import 'package:dotify_app/model/music_model.dart';
import 'package:dotify_app/pages/now_playing_page.dart';
import 'package:flutter/material.dart';

class ArtistPage extends StatelessWidget {
  final MainModel model;

  const ArtistPage({Key key, this.model}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Background(
      scaffold: Scaffold(
        backgroundColor: Colors.white70,
        appBar: PreferredSize(
          child: Padding(
            padding: const EdgeInsets.all(10.0),
            child: Stack(
              children: <Widget>[Container(
                color: Colors.transparent,height: 300,),
                Container(decoration: BoxDecoration(
                  // color: Colors.black26,
                  
                  image: DecorationImage(colorFilter: ColorFilter.mode(Colors.blueGrey[500], BlendMode.overlay),
                  
                    image: AssetImage('assets/image/2.png'),fit: BoxFit.cover)

                ),
                
                  height: 260,
                  width: MediaQuery.of(context).size.width,
                    child: Column(
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.only(
                          left: 10,
                          top: 25,
                        ),
                        child: Row(
                          children: <Widget>[
                            IconButton(
                              icon: Image.asset(
                                  'assets/icon/back_btn_light@2x.png'),
                              onPressed: () {
                                Navigator.pop(context);
                              },
                            ),
                            SizedBox(
                              width: 48,
                            ),
                            Column(
                              children: <Widget>[
                                Text(
                                  'Artist',
                                  style: TextStyle(color: Colors.white),
                                ),
                                Text(
                                  'Fawkessy in Sawkessy',
                                  style: TextStyle(color: Colors.white),
                                )
                              ],
                            )
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Stack(
                        children: <Widget>[
                          CircleAvatar(
                            radius: 40,
                            backgroundImage: AssetImage('assets/image/2.png'),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 30,
                      ),
                     
                    ],
                  ),
                 
                    
                  ),
                  
                  Padding(
                        padding: const EdgeInsets.only(left: 80,top: 220),
                        child: Row(
                          children: <Widget>[
                            Image.asset('assets/icon/heart_song_btn.png'),
                            SizedBox(width: 12,),
                            Image.asset('assets/icon/play_artist_btn.png'),
                            SizedBox(width: 12,),
                            Image.asset('assets/icon/share_music_btn.png')
                          ],
                        ),
                      )
              ],
            ),
          ),
          preferredSize: Size(200, 300),
        ),
        body: CustomScrollView(
          slivers: <Widget>[
            SliverList(
              delegate: SliverChildListDelegate([
                Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Text('LATEST RELEASE'),
                ),
                Divider()
              ]),
            ),
            SliverList(
              delegate: SliverChildListDelegate([
                ListTile(
                  leading: Image.asset(
                    'assets/image/29.png',
                    height: 50,
                    width: 40,
                  ),
                  title: Text('Diamond Platnumz'),
                  subtitle: Text('10 Songs,May 15 2016'),
                  trailing: Padding(
                    padding: const EdgeInsets.only(right: 10),
                    child: Image.asset('assets/icon/view_album_btn@2x.png'),
                  ),
                ),
                Divider()
              ]),
            ),
            SliverList(
              delegate: SliverChildListDelegate([
                Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Text('POPULAR'),
                ),
                Divider()
              ]),
            ),
            SliverList(
              delegate: SliverChildListDelegate([
                ListTile(
                  leading: Text('1'),
                  title: Text(
                    'Every Day',
                    style: TextStyle(color: Colors.deepOrange),
                  ),
                  subtitle: Text('7342567'),
                  trailing: Image.asset('assets/icon/more_info_btn@2x.png'),
                ),
                Divider(),
                
              ]),
            ),
            // SliverList(
            //       delegate: SliverChildBuilderDelegate(
            //           (BuildContext context, int index) {
            //         return ArtistsSong(
            //           album: album, index: index,
            //         );
            //       }, childCount: music.length),
            //     )
          ],
        ),
        bottomNavigationBar: BottomAppBar(
            child: FlatButton(
          child: ListTile(
            leading: CircleAvatar(
              backgroundImage: AssetImage('assets/image/32.png'),
            ),
            title: Text(" All The Trees  Their "),
            subtitle: Text('Drey Stevens'),
            trailing: Image.asset('assets/icon/play_btn_pop_up@2x.png'),
          ),
          onPressed: () {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (BuildContext context) => NowPlaying( playing: musics.removeLast(), model: model,)));
          },
        )),
      ),
      sigmaX: 60,
      sigmaY: 60,
    );
  }
}
