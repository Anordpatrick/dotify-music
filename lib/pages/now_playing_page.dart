import 'package:audioplayers/audioplayers.dart';
import 'package:dotify_app/data/scoped_modal/main.dart';
import 'package:dotify_app/model/background.dart';
import 'package:dotify_app/model/music_model.dart';
import 'package:dotify_app/model/playlist.dart';
import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:share/share.dart';

class NowPlaying extends StatefulWidget {
  final Music playing;
  final int index;
  final MainModel model;
  const NowPlaying(
      {Key key, this.index, @required this.playing, @required this.model})
      : super(key: key);
  @override
  _NowPlayingState createState() => _NowPlayingState();
}

class _NowPlayingState extends State<NowPlaying> {
  Map<String, dynamic> song;
  Duration _duration = new Duration();
  Duration _position = new Duration();

  @override
  void initState() {
    initPlayer();
    super.initState();
  }

  void initPlayer() {
    AudioPlayer advancedPlayer = widget.model.audioPlayerInstance;
    print(advancedPlayer);

    advancedPlayer.onDurationChanged.listen((onData) {
      setState(() {
        _duration = onData;
      });
    });

    advancedPlayer.onAudioPositionChanged.listen((onData) {
      setState(() {
        _position = onData;
      });
    });
  }

   void seekToseconds(int second){
         AudioPlayer advancedPlayer = widget.model.audioPlayerInstance;
         Duration newDuration = new Duration(seconds: second);
         advancedPlayer.seek(newDuration);

   }

  Widget build(BuildContext context) {
    return ScopedModelDescendant(
      builder: (BuildContext context, Widget child, MainModel model) {
        return Background(
          scaffold: Scaffold(
            backgroundColor: Colors.white54,
            body: SingleChildScrollView(
              child: Padding(
                padding: const EdgeInsets.only(top: 35),
                child: Stack(
                  children: <Widget>[
                    Column(
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: Container(
                            // color: Colors.blueGrey,
                            decoration: BoxDecoration(
                                image: DecorationImage(
                                    image: AssetImage('assets/image/asha.jpg'),
                                    fit: BoxFit.cover)),
                            height: MediaQuery.of(context).size.height * 2 / 4,
                            child: Align(
                              alignment: Alignment.topCenter,
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Column(
                                  children: <Widget>[
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: <Widget>[
                                        InkWell(
                                          onTap: () {
                                            Navigator.pop(context);
                                          },
                                          child: Image.asset(
                                              'assets/icon/dismiss_music@2x.png',
                                              height: 30,
                                              width: 30),
                                        ),
                                        Image.asset(
                                            'assets/icon/music_info@2x.png',
                                            height: 30,
                                            width: 30),
                                      ],
                                    ),
                                    Padding(
                                      // slider
                                      child: Slider(
                                        activeColor: Colors.deepOrange,
                                        inactiveColor: Colors.white,
                                        onChanged: (double value) {
                                          setState(() {
                                            setState(() {
                                             seekToseconds(value.toInt());
                                             value = value; 
                                            });
                                          });
                                        },
                                        value: _position.inSeconds.toDouble(),
                                        min: 0.0,
                                        max: _duration.inSeconds.toDouble(),
                                      ),
                                      padding: EdgeInsets.only(top: 220),
                                    )
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ),
                        ListTile(
                          leading: Text(((_position.inSeconds/60).floor()).toString()+':'+((_position.inSeconds%60).round()).toString()),
                          trailing: Text(_duration.inMinutes.toString()+':'+((_duration.inSeconds)-(_duration.inMinutes*60)).toString()),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 35, right: 35),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Image.asset('assets/icon/favorite_song_btn.png'),
                              SizedBox(
                                width: 8,
                              ),
                              Column(
                                children: <Widget>[
                                  Text(widget.playing.song),
                                  Text(widget.playing.name)
                                ],
                              ),
                              SizedBox(
                                width: 8,
                              ),
                              InkWell(
                                  onTap: () {
                                    _showMore();
                                  },
                                  child: Image.asset(
                                      'assets/icon/more_info_btn@2x.png'))
                            ],
                          ),
                        ),
                        SizedBox(
                          height: 15,
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 25, right: 25),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Image.asset('assets/icon/shuffle_btn.png'),
                              Image.asset('assets/icon/rewind_btn.png'),
                              model.isPlayed
                                  ? IconButton(
                                      iconSize: 70,
                                      icon: Image.asset(
                                          'assets/icon/play_btn.png'),
                                      onPressed: () {
                                        // audioCache.play('fileName');
                                        model.setisPlayed(false);
                                        model.play();
                                      },
                                    )
                                  : IconButton(
                                      iconSize: 70,
                                      icon: Image(
                                        color: Colors.black,
                                        image: AssetImage(
                                            'assets/icon/pause_btn@2x.png'),
                                      ),
                                      color: Colors.black,
                                      onPressed: () {
                                        // advancedPlayer.pause();

                                        model.setisPlayed(true);
                                        model.pause();
                                      },
                                    ),
                              InkWell(
                                  onTap: () {},
                                  child: Image.asset(
                                      'assets/icon/fastforward_btn.png')),
                              Image.asset('assets/icon/repeat_btn.png')
                            ],
                          ),
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 130),
                          child: InkWell(
                            onTap: () {
                              initPlayer();
                            },
                            child: Row(
                              children: <Widget>[
                                Icon(Icons.volume_up),
                                Text('Headphones')
                              ],
                            ),
                          ),
                        )
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ),
          sigmaX: 30,
          sigmaY: 30,
        );
      },
    );
  }

  void _showMore() {
    showModalBottomSheet(
      builder: (BuildContext context) {
        return Container(
          height: 400,
          color: Colors.deepOrange,
          child: Padding(
            padding: const EdgeInsets.all(10.0),
            child: Column(
              children: <Widget>[
                ListTile(
                  leading: CircleAvatar(
                    backgroundImage: AssetImage('assets/image/30.png'),
                  ),
                  title: Text('Fever'),
                  subtitle: Text('Wizkid'),
                ),
                SizedBox(
                  height: 20,
                ),
                InkWell(
                  onTap: () {
                    Share.share('drey');
                  },
                  child: Row(
                    children: <Widget>[
                      Icon(Icons.share),
                      SizedBox(
                        width: 8,
                      ),
                      Text('Share')
                    ],
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Row(
                  children: <Widget>[
                    Icon(Icons.favorite),
                    SizedBox(
                      width: 8,
                    ),
                    Text('Favourites'),
                  ],
                ),
                SizedBox(
                  height: 10,
                ),
                Row(
                  children: <Widget>[
                    Icon(Icons.comment),
                    SizedBox(
                      width: 8,
                    ),
                    Text('Comments'),
                  ],
                ),
                SizedBox(
                  height: 10,
                ),
                InkWell(
                  onTap: () {
                    final Playlist playlist = new Playlist(
                        songname: widget.playing.song,
                        artistname: widget.playing.name);
                    playlists.add(playlist);
                  },
                  child: Row(
                    children: <Widget>[
                      Icon(Icons.playlist_add),
                      SizedBox(
                        width: 8,
                      ),
                      Text('Add to Playlist')
                    ],
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Row(
                  children: <Widget>[
                    Icon(Icons.play_arrow),
                    SizedBox(
                      width: 8,
                    ),
                    Text('Play Next')
                  ],
                )
              ],
            ),
          ),
        );
      },
      context: context,
    );
  }
}
